package org.nrg.dicom.dicomedit.pixeledit.validation;

public class DataValidationException extends Exception {
    public DataValidationException(String msg) {
        super(msg);
    }
}
