package org.nrg.dicom.dicomedit;

import org.nrg.dicom.mizer.exceptions.MizerException;

import java.util.List;

/**
 * This processor strips all end-line comments out of the script.
 * Scan for comment-start string "//".
 * Lines can contain URLs. If the character before "//" is ":" then don't match.
 * Comments in strings (text between double-quotes) are retained.
 * Strings are not nested, ""// comment"" parses as string-comment-string.
 */
public class ScriptProcessorRemoveEndLineComments implements ScriptDirectiveProcessor {
    public ScriptProcessorRemoveEndLineComments() {
    }

    @Override
    public DE6Script process(DE6Script script) {
        return null;
    }

    /**
     * strips all end-line comments
     *
     * @param lines list of lines to strip.
     * @return list of stripped lines.
     */
    @Override
    public List<String> process(List<String> lines) throws MizerException {
        return new InlineCommentRemover().removeInLineComments(lines);
    }
}
